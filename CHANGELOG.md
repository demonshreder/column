# Change Log

All notable changes to this project will be documented in this file. This change log follows the conventions of [keepachangelog.com](http://keepachangelog.com/)

## [0.28.1] - 2023-05-26

## Added

- Update gitignore to add the files created by eastwood, clj-kondo, sonarqube

## [0.28.0] - 2023-05-01

- Happy May Day!!

## Fixed

- Typo in page title in home.clj causing the template to break

## Changed

- Cookie secret is now auto-generated 16 bytes long byte array

## [0.27.0] - 2023-04-30

## Changed

- Update dependencies

## [0.26.0] - 2023-01-24

## Changed

- Update dependencies

## [0.25.0] - 2022-08-31

## Added

- New interceptor chain - logged-in-interceptors

## Changed

- Update bulma dependencies
- Make the homepage

## Fixed

- Redirect when not logged in
- Fixed logout

## [0.24.1] - 2022-08-10

## Fixed

- Fixed password matching using wrong params. Registration works

## [0.24.0] - 2022-08-10

## Added

- Added migration to create the PSQL "uuid-ossp" extension before creating the user model

## Changed

- User model primary key is now UUID, with username and email as unique
- Changed the migrations number from within hundreds to thousands - 00x -> 000x
- Upgraded dependencies
- User model migration is now 0004

## [0.23.0] - 2022-07-10

## Changed

- Upgraded jetty dependencies to the last supported version 9.48.x
- Upgraded dependencies

## [0.22.0] - 2022-05-20

## Changed

- Change auth module code according to suggestion from `lein kibit`

## [0.21.0] - 2022-05-16

## Changed

- Updated deps

## [0.20.0] - 2022-04-12

## Fixed

- Typo in generated project.clj

### Changed

- Upgraded clojure
- Upgraded dependencies

## [0.19.0] - 2022-01-15

### Added

- Enforced a standard Uberjar name for deployment ease
- Dockerfile to build stuff
- Docker build & run instructions in README

### Changed

- Updated Pedestal libs & few other dependencies

## [0.18.2] - 2022-01-11

### Changed

- Add static assets, LICENSE & README to Gitlab artifacts

## [0.18.1] - 2022-01-11

### Changed

- Add appropriate naming to Gitlab artifacts

## [0.18.0] - 2022-01-11

### Add

- .gitlab-ci.yml file

### Changed

- Add an if statement to help with Gitlab CI/CD
- Change logback config to ensure log files start with name of project

## [0.17.0] - 2022-01-09

### Changed

- Change default server to Jetty as it has no active vulnerabilities and is under active development

## [0.16.1] - 2022-01-08

### Changed

- Update README within new Projects

## [0.16.0] - 2021-12-26

### Changed

- Update transitive dependencies to ensure zero vulnerability against knowns CVE

## [0.15.1] - 2021-12-26

### Changed

- Updated PSQL dependencies

## [0.15.0] - 2021-12-26

### Changed

- Updated dependencies

## [0.14.0] - 2020-12-29

### Added

- Notification controls for success & failure flash messages
- Detailed out fine points in TODO
- /logout - Clears sessions as expected

### Changed

- Moved common interceptors from service.clj into interceptors.clj
- Moved user specific routes from service.clj to auth/routes.clj
- Registration session now matches login session
- Update Bulma CSS from 0.8.1 to 0.9.1

### Fixed

- Registration not working due to a typo

### Updated

- Updated org.postgresql/postgresql from 42.2.16 to 42.2.18
- Updated buddy-hashers from 1.4.0 - 1.7.0

## [0.13.0] - 2020-09-13

### Added

- Authentication module has been made separate
- Creation of users with username and/or email
- Logging in with session keys as per buddy-auth
- Checking if user is logged in
- Primitives for ACL / RBAC using set - Thanks to Ragulkanth
- Interceptor for authenticated routes

## [0.12.0] - 2020-09-03

### Changed

- Updated Pedestal from 0.5.7 to 0.5.8
- Updated Cheshire from 5.9.0 to 5.10.0
- Updated Postgresql from 42.2.12 to 42.2.16
- Updated hikari-cp from 2.12.2 to 2.12.3

## [0.11.0] - 2020-05-08

### Added

- Username field to registration
- Necessary SQL queries to roles & users

### Fixed

- CSRF token interceptor during GET requests
- Missing data type in 001-create-roles.edn migration
- Wrong use of keyword in table column

### Changed

- Removed unnecessary div for hidden form values
- Updated bulma.css from 0.7.5 to 0.8.2
- site.clj/form-control to site.clj/form-control-input

## [0.10.2] - 2020-05-04

### Fixed

- Typo in home.clj
- Corrected SQL for db/users.sql

## [0.10.1] - 2020-05-04

### Changed

- Updated dependencies for template's project.clj

### Fixed

- Service test served from wrong path
- Test conditions

## [0.10.0] - 2020-05-04

Pedestal's CSRF protection boiler plate

### Added

- CSRF Anti-forgery interceptor to service.clj
- CSRF token hidden value field for forms to site.clj

### Changed

- Reorder authentication from site/auth.clj to a separate module

## [0.9.0] - 2020-04-24

### Added

- Migrations for roles, roles data

## [0.8.1] - 2020-02-11

### Added

- config/logback.xml for logging

## [0.8.0] - 2020-01-22

### Changed

- Changed HTTP server from Jetty to Immutant
- template/project.clj - Excluded 'media'and 'logs' directories from uberjar
- template/project.clj - Add library for Immutant and updated existing deps

## [0.7.7] - 2020-01-20

### Added

- Added separation of param values for site.clj templates

## [0.7.6] - 2019-12-23

### Fixed

- auth.clj not bundling

## [0.7.5] - 2019-12-22

### Changed

- auth.clj file permission

## [0.7.4] - 2019-12-21

### Added

- CSP policy - Allows JS from origin, including inline

## [0.7.3] - 2019-12-04

### Added

- logback.xml config to suppress hikari-cp messages

## [0.7.2] - 2019-10-17

### Fixed

- template/project.clj - Added io.pedestal/service-tools missing dependency as column used service-tools.dev/watch to auto-load server while in dev mode

### Changed

- template/project.clj deps - Updated org.slf4j, cheshire, hugsql, org.postgresql

## [0.7.1] - 2019-09-19

### Changed

- server.clj - Moved invocation of pedestal.dev/watch into run-dev

## [0.7.0] - 2019-07-23

### Added

- site.clj - Added dynamic controls for hidden value, textarea, date control
- service.clj - Add json-interceptors for automatically encoding / decoding edn into JSON
- template/project.clj - Add [cheshire "5.8.1"] as a dependency

### Fixed

- Wrong dates in the changelog

### Changed

- template/project.clj deps - Update [org.clojure/clojure] from "1.10.0" to "1.10.1"
- template/project.clj deps - Update [io.pedestal/pedestal.service] from "0.5.5" to "0.5.7"
- template/project.clj deps - Update [io.pedestal/pedestal.jetty] from "0.5.5" to "0.5.7"
- template/project.clj deps - Update [org.postgresql/postgresql] from "42.2.5" to "42.2.6"
- template/project.clj deps - Update [io.pedestal/pedestal.service-tools] (dev only) from "0.5.5" to "0.5.7"
- template/project.clj deps - Update [hikari-cp] from "2.7.1" to "2.8.0"
- template/project.clj deps - Update [buddy/buddy-auth] from "2.1.0" to "2.2.0"
- template/project.clj deps - Update [buddy/buddy-hashers] from "1.3.0" to "1.4.0"
- site.clj - Update bulma.css from [0.7.4] to [0.7.5]

## [0.6.1] - 2019-06-04

### Fixed

- Excessive memory usage while being refreshed due to a function being called via 'def'

## [0.6.0] - 2019-06-04

### Added

- Made pedestal.io.service-tools.dev/watch accessible under server namespace
- Added hiccup templates for bulma based HTML controls

## [0.5.0] - 2019-04-12

### Added

- Buddy auth session & authorization interceptors
- Buddy unauthorized exception's handler sample
- Buddy 'authenticated?' sample
- Function to create new user in site.auth

### Changed

- Annonymized jdbc.edn & removed unnecessary comment

## [0.4.0] - 2019-04-07

### Changed

- JDBC single URL is now datasource options for hikari-cp database connection pool
- db/jdbc is now a hikari-cp connection pool object
- Ragtime now uses the hikari-cp connection pool

### Removed

- Mount based db 'conn' as it wasn't working & isn't needed anymore

## [0.3.2] - 2019-04-01

### Fixed

- Multiple versions of JDBC URL in db.clj

### Changed

- User table now has proper fields - id, email, password, created_at, deleted_at, deleted

## [0.3.1] - 2019-03-19

### Fixed

- Documentation for column method in column.clj
- .gitignore not being found

## [0.3.0] - 2019-03-19

### Added

- Add 'Why Pedestal?' section to README
- buddy-auth interceptors

## [0.2.0] - 2019-03-18

### Added

- Cookie based session support
- Add 'secret.edn' for session secrets

### Changed

- Update README.md

### Fixed

- project.clj missing
- service.clj, home.clj, site.clj, README.md wrong namespace

## [0.1.1] - 2019-03-13

### Removed

- Test files

## [0.1.0] - 2019-03-13

### Added

- Files for the new template.
