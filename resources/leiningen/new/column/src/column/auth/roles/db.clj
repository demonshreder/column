(ns {{name}}.auth.roles.db
  (:require [hugsql.core :as hugsql]))

(hugsql/def-db-fns "{{name}}/auth/roles/roles.sql")
