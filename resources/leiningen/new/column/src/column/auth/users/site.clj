(ns {{name}}.auth.users.site
    (:require [ring.util.response :as ring-resp]
              [{{name}}.site :as site]
              [{{name}}.auth :as auth]
              [{{name}}.auth.users.forms :as forms]
              [{{name}}.auth.users :as auth.users]
              [buddy.auth :as ba]))

(defn login-page [request]
  (ring-resp/response
   (site/base
    request
    "Login"
    (forms/login-control request))))

(defn login-post [request]
  (let [data (:form-params request)
        user (auth/authenticate-username-password data)
        session (:session request)]
    (if (nil? user)
      (ring-resp/response "Username or password is incorrect")
      (let [updated-session (assoc session
                                   :identity (keyword (:username user))
                                   :role (keyword (:role user)))]
        (assoc (ring-resp/redirect "/")
               :session updated-session)))))

(defn logout
  "Logs out the authenticated session"
  [request]
  (-> (ring-resp/redirect "/")
      (dissoc :session)
      (dissoc :identity)))

(defn register-page [request]
  (ring-resp/response
   (site/base
    request
    "Register"
    (forms/register-control request))))

(defn register-post [request]
  (let [data (:form-params request)
        session (:session request)]
    (if (= (:password data) (:confirm-password data))
      (if (auth.users/user-exists? data)
        (do (auth.users/new-user data)
            (let [updated-session (assoc session
                                         :identity (keyword (:username data))
                                         :role :user)]
              (assoc (ring-resp/redirect "/")
                     :session updated-session)))
        (ring-resp/response "Username or email already taken"))
      (ring-resp/response "Password doesn't match"))))
