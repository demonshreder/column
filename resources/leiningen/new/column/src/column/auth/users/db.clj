(ns {{name}}.auth.users.db
  (:require [hugsql.core :as hugsql]))

(hugsql/def-db-fns "{{name}}/auth/users/users.sql")
