(ns {{name}}.site.home
  (:require [hiccup.core :as hiccup]
            [ring.util.response :as ring-resp]
            [{{name}}.site :as site]
            [{{name}}.auth :as {{name}}.auth]
            [buddy.auth :as auth]))

(defn home-page [request]
  (ring-resp/response
   (site/base request
              "{{name}}"
              [:div.has-text-centered
              [:div.title.is-1
               "{{name}}"]
              [:p.subtitle  (if (auth/authenticated? request)
               "You are logged in"
               "You are not logged in")]])))
