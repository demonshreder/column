(defproject {{name}} "0.1.0-SNAPSHOT"
  :description "FIXME "
  :url "FIXME"
  :license {:name "AGPLv3 with exceptions"
            :url "https://www.gnu.org/licenses/agpl.txt"}
  :dependencies [[org.clojure/clojure "1.11.1"]
                 [io.pedestal/pedestal.service "0.5.10"]
                 [io.pedestal/pedestal.service-tools "0.5.10"
                  :exclusions [[ring/ring-core]]]
                 [io.pedestal/pedestal.jetty "0.5.10"
                  :exclusions [[org.eclipse.jetty/jetty-server]
                               [org.eclipse.jetty.websocket/websocket-api]
                               [org.eclipse.jetty.websocket/websocket-server]
                               [org.eclipse.jetty/jetty-alpn-server]
                               [org.eclipse.jetty/jetty-servlet]
                               [org.eclipse.jetty.http2/http2-server]
                               [org.eclipse.jetty.websocket/websocket-servlet]]]
                 [ch.qos.logback/logback-classic "1.4.7" :exclusions                  [org.slf4j/slf4j-api]]
                 [org.slf4j/jul-to-slf4j "2.0.7"]
                 [org.slf4j/jcl-over-slf4j "2.0.7"]
                 [org.slf4j/log4j-over-slf4j "2.0.7"]
                 [hiccup "2.0.0-alpha2"]
                 ;; [org.clojure/clojurescript "1.10.866"]
                 [hikari-cp "3.0.1"]
                 [ragtime "0.8.1"]
                 [org.postgresql/postgresql "42.6.0"]
                 [com.layerware/hugsql "0.5.3"]
                 [buddy/buddy-auth "3.0.323"]
                 [buddy/buddy-hashers "1.8.158"]
                 [cheshire "5.11.0"]
                 [org.eclipse.jetty.websocket/websocket-api "9.4.51.v20230217"]
                 [org.eclipse.jetty.websocket/websocket-server "9.4.51.v20230217"]
                 [org.eclipse.jetty/jetty-alpn-server "9.4.51.v20230217"]
                 [org.eclipse.jetty/jetty-servlet "9.4.51.v20230217"]
                 [org.eclipse.jetty.http2/http2-server "9.4.51.v20230217"]
                 [org.eclipse.jetty.websocket/websocket-servlet "9.4.51.v20230217"]
                 [ring/ring-core "1.10.0"]
                 [joda-time "2.12.5"]]
  :min-lein-version "2.0.0"
  :resource-paths ["config", "resources"]
  ;; If you use HTTP/2 or ALPN, use the java-agent to pull in the correct alpn-boot dependency
  ;:java-agents [[org.mortbay.jetty.alpn/jetty-alpn-agent "2.0.5"]]
  :profiles {:dev {:aliases {"run-dev" ["trampoline" "run" "-m" "{{name}}.server/run-dev"]
                             "migrate"  ["run" "-m" "{{name}}.server/migrate"]
                             "rollback" ["run" "-m" "{{name}}.server/rollback"]}
                   :dependencies [[io.pedestal/pedestal.service-tools "0.5.10"]]}
             :uberjar {:uberjar-name "{{name}}-standalone-release.jar"
                       :jar-exclusions [#"/media/",#"/logs/"]
                       :uberjar-exclusions [#"/media/",#"/logs/"]
                       :aot [{{name}}.server]}}
  :main ^{:skip-aot true} {{name}}.server)
