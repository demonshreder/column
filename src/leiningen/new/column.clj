(ns leiningen.new.column
  (:require [leiningen.new.templates :refer [renderer name-to-path ->files]]
            [leiningen.core.main :as main]
            [crypto.random :as crypto.random]))

(def render (renderer "column"))

(defn column
  "column is a starter template for Pedestal based webapps"
  [name]
  (let [data {:name name
              :sanitized (name-to-path name)
              :secret (vec (crypto.random/bytes 16))}]
    (main/info "Generating fresh 'lein new' column project.")
    (->files data
             ["project.clj" (render "project.clj" data)]
             ["README.md" (render "README.md" data)]
             ["ARCHITECTURE.md" (render "ARCHITECTURE.md" data)]
             ["CHANGELOG.md" (render "CHANGELOG.md" data)]
             ["CONTRIBUTING.md" (render "CONTRIBUTING.md" data)]
             [".gitignore" (render "gitignore" data)]
             [".gitlab-ci.yml" (render "gitlab-ci.yml" data)]
             ["Dockerfile" (render "Dockerfile" data)]
             ["config/logback.xml" (render "config/logback.xml" data)]
             ["resources/env/jdbc.edn" (render "resources/env/jdbc.edn" data)]
             ["resources/env/secret.edn" (render "resources/env/secret.edn" data)]
             ["resources/migrations/0001-create-roles.edn" (render "resources/migrations/0001-create-roles.edn" data)]
             ["resources/migrations/0002-create-roles-data.edn" (render "resources/migrations/0002-create-roles-data.edn" data)]
             ["resources/migrations/0003-create-uuid-extension.edn" (render "resources/migrations/0003-create-uuid-extension.edn" data)]
             ["resources/migrations/0004-create-users.edn" (render "resources/migrations/0004-create-users.edn" data)]
             ["src/{{sanitized}}/server.clj" (render "src/column/server.clj" data)]
             ["src/{{sanitized}}/service.clj" (render "src/column/service.clj" data)]
             ["src/{{sanitized}}/site.clj" (render "src/column/site.clj" data)]
             ["src/{{sanitized}}/site/home.clj" (render "src/column/site/home.clj" data)]
             ["src/{{sanitized}}/db.clj" (render "src/column/db.clj" data)]
             ["src/{{sanitized}}/interceptors.clj" (render "src/column/interceptors.clj" data)]
             ["src/{{sanitized}}/auth.clj" (render "src/column/auth.clj" data)]
             ["src/{{sanitized}}/auth/routes.clj" (render "src/column/auth/routes.clj" data)]
             ["src/{{sanitized}}/auth/users.clj" (render "src/column/auth/users.clj" data)]
             ["src/{{sanitized}}/auth/users/forms.clj" (render "src/column/auth/users/forms.clj" data)]
             ["src/{{sanitized}}/auth/users/site.clj" (render "src/column/auth/users/site.clj" data)]
             ["src/{{sanitized}}/auth/users/db.clj" (render "src/column/auth/users/db.clj" data)]
             ["src/{{sanitized}}/auth/users/users.sql" (render "src/column/auth/users/users.sql" data)]
             ["src/{{sanitized}}/auth/roles.clj" (render "src/column/auth/roles.clj" data)]
             ["src/{{sanitized}}/auth/roles/db.clj" (render "src/column/auth/roles/db.clj" data)]
             ["src/{{sanitized}}/auth/roles/roles.sql" (render "src/column/auth/roles/roles.sql" data)]
             ["test/{{sanitized}}/service_test.clj" (render "test/column/service_test.clj" data)])))
