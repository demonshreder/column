(defproject column/lein-template "0.28.1"
  :description "column sits on top of pedestal to support your projects "
  :url "https://gitlab.com/demonshreder/column"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath Exception"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :eval-in-leiningen true
  :dependencies [[crypto-random "1.2.1"]]
  :plugins [[lein-codox "0.10.8"]]
  :scm {:name "git" :url "https://gitlab.com/demonshreder/column"})
