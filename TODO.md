# TODO

## 12factor

- Health check
- Port env var
- DB config env var
- Remove log files

## Homepage

- Add a default homepage

## Routes

- Detached routes file

## File Upload

- Storage customizable file upload

## User

- Logout
- More work on RBAC needed?
- Users Admin form needed
- Add sessions table
- Add max logins counter

## Wiki / User Guide

- API
- Architectural overview
