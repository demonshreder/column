# column

[![Clojars Project](https://img.shields.io/clojars/v/column/lein-template.svg)](https://clojars.org/column/lein-template)

column is a leiningen template for pedestal based web applications.

## Usage

lein new column <name>

## Objectives

- Django Framework for Clojure using Pedestal without the Framework part.
- Sample code to copy paste while using Pedestal
- Quick start template for Pedestal

## Why Pedestal?

Official README - https://github.com/pedestal/pedestal/blob/master/README.md#notable-capabilities

Reddit discussion - https://www.reddit.com/r/Clojure/comments/4jmaod/ask_rclojure_secure_web_frameworks_for_building/

## Components & Configurations

- Hikari-cp
- Postgresql
- Hiccup
- HugSQL
- Mount
- Encrypted cookie based session
- Updated separate dependencies from Pedestal

## License

Copyright © 2019 Kamalavelan

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
